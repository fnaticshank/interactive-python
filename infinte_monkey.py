# TODO:
# Improve the program in the self check by keeping letters that are correct and
# only modifying one character in the best string so far. This is a type of
# algorithm in the class of 'hill climbing' algorithms, that is we only keep
# the result if it better than the previous one.


import random


def generateOne(strlen):
    alphabet = 'abcdefghijklmnopqrstuvwxyz '
    res = ''
    for i in range(strlen):
        res = res+alphabet[random.randrange(27)]

    return res


def score(goal, test_string):
    numSame = 0
    for i in range(len(goal)):
        if goal[i] == test_string[i]:
            numSame += 1
    return numSame / len(goal)


def run():
    try:
        goalstring = 'methinks it is like a weasel'
        newstring = generateOne(28)
        best = 0
        newscore = score(goalstring, newstring)
        beststring = ''
        while newscore < 1:
            if newscore > best:
                print(newscore, newstring)
                best = newscore
                beststring = newstring
            newstring = generateOne(28)
            newscore = score(goalstring, newstring)

    except KeyboardInterrupt:
        print('\n --- final result --- \n')
        print(best, beststring)

run()
