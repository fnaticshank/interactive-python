"""
A simple soltion to calculate and store the results for the minimum number of
coins.
"""
def fmincoins(coinValueList, change, memo):
    mincoins = change
    if change in coinValueList:
        # the change value is equal to one the coin value
        memo[change] = 1
        return 1

    elif memo[change] > 0:
        # this is the check which prevents unnecessary recursive calls.
        # if the value of memo at index change is greater than zero, it means
        # the value has been previously calculated so just return it
        return memo[change]

    else:
        # final step wherein we calculate recursively for the min number of
        # coins for a given change
        for i in [c for c in coinValueList if c <= change]:
            numcoins = 1 + fmincoins(coinValueList, change-i, memo)

            if numcoins < mincoins:
                mincoins = numcoins
                memo[change] = mincoins
    return mincoins

#print fmincoins([1,5,10,25], 63, [0]*64)


def dpmakechange(coinValueList, change, mincoins, coinsused):
    for cents in range(change+1):
        coincount = cents
        newcoin = 1
        for j in [c for c in coinValueList if c <= cents]:
            if mincoins[cents-j] + 1 < coincount:
                coincount = mincoins[cents-j] + 1
                newcoin = j
        mincoins[cents] = coincount
        coinsused[cents] = newcoin
    return mincoins[change]

def printcoins(coinsused, change):
    coin = change
    while coin > 0:
        thiscoin = coinsused[coin]
        print thiscoin
        coin = coin - thiscoin


def main():
    change = 73
    coinlist = [1, 5, 10, 21, 25]
    coincount = [0]*(change+1)
    coinsused = [0]*(change+1)

    print 'Amount: ', change
    print 'Coins used: ', dpmakechange(coinlist, change, coincount, coinsused)
    printcoins(coinsused, change)

main()
