class termcolors:
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    GREY = '\033[30m'
    CYAN = '\033[96m'

    HIGHLIGHT_Red = '\033[1;41m'
    HIGHLIGHT_Green = '\033[1;42m'
    HIGHLIGHT_Yellow = '\033[1;43m'
    HIGHLIGHT_Blue = '\033[1;44m'
    HIGHLIGHT_Magenta = '\033[1;45m'
    HIGHLIGHT_Cyan = '\033[1;46m'
    HIGHLIGHT_Grey = '\033[1;47m'

