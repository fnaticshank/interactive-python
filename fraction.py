from __future__ import division
from colorprompt import termcolors


def gcd(m, n):
    while m % n != 0:
        m, n = n, m % n
    return n


class NotAnIntegerError(Exception):

    def __init__(self):
        self.value = Exception

    def __str__(self):
        return repr(self.value)


class Fraction:

    def __init__(self, top, bottom):

        try:
            if isinstance(top, int) and isinstance(bottom, int):
                common_divisor = gcd(top, bottom)
                self.num = top//common_divisor
                self.den = bottom//common_divisor
            else:
                raise NotAnIntegerError
        except NotAnIntegerError:
            print(termcolors.HIGHLIGHT_Red + termcolors.BOLD +
                  'NotAnIntegerError:' + termcolors.ENDC +
                  ' The following other can only contain integers')
            exit()

    def __str__(self):
        return str(self.num)+"/"+str(self.den)

    def __add__(self, other):
        newnum = self.num * other.den + self.den * other.num
        newden = self.den * other.den

        #return Fraction(newnum, newden)
        return NotImplemented

    def __sub__(self, other):
        newnum = self.num * other.den - self.den * other.num
        newden = self.den * other.den

        return Fraction(newnum, newden)

    def __mul__(self, other):
        newnum = self.num * other.num
        newden = self.den * other.den

        return Fraction(newnum, newden)

    def __truediv__(self, other):
        newnum = self.num * other.den
        newden = self.den * other.num

        return Fraction(newnum, newden)

    def __eq__(self, other):
        first_num = self.num * other.den
        second_num = other.num * self.den

        return first_num == second_num

    def __iadd__(self, other):
        newnum = self.num * other.den + self.den * other.num
        newden = self.den * other.den
        print(termcolors.WARNING + 'calling --- __iadd__' + termcolors.ENDC)

        return Fraction(newnum, newden)

    def __radd__(self, other):
        newnum = self.num * other.den + self.den * other.num
        newden = self.den * other.den
        print(termcolors.WARNING + 'calling --- __radd__' + termcolors.ENDC)

        return Fraction(newnum, newden)

    def get_num(self):
        return self.num

    def get_den(self):
        return self.den

f1 = Fraction(1, 3)
f2 = Fraction(4, 8)

print 'nums:', f1, ',', f2
print 'equl:', f1 == f2
print 'add:', f1+f2
print 'sub:', f1-f2
print 'mul:', f1*f2
print 'div:', f1/f2
