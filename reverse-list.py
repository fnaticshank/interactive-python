# a recursive function that reverses a list

def reverse(l):
    # this method removes the last element and then adds the remaining list to
    # it
    if not l: return []
    return [l[-1]] + rev(l[:-1])

# this lambda function pops the first element and the first element to the
# remaining list
backwards = lambda l : (backwards (l[1:]) + l[:1] if l else [])

