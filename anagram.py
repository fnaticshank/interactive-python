"""
Anagram detection program.
At a very basic level, 2 strings are anagrams if they are of equal length and
have the same set of characters.
"""

def anagram1(s1, s2):
    """O(n^2) - Normal method for comparing strings, uses 2 loops"""
    alist = list(s2)
    pos1 = 0
    stillOk = True

    while pos1 < len(s1) and stillOk:
        pos2 = 0
        found = False
        while pos2 < len(alist) and not found:
            if s1[pos1] == alist[pos2]:
                found = True
            else:
                pos2 = pos2 + 1

        if found:
            alist[pos2] = None
        else:
            stillOk = False
        pos1 = pos1 + 1

    return stillOk


def anagram2(s1, s2):
    """
    O(n log n) typically depends on the python's sort method which uses tim sort
    internally. A sort and compare approach.
    """
    l1 = list(s1)
    l2 = list(s2)
    l1.sort()
    l2.sort()

    pos = 0
    matches = True

    while pos < len(s1) and matches:
        if l1[pos] == l2[pos]:
            pos = pos + 1
        else:
            matches = False

    return matches


def anagram3(s1, s2):
    """
    O(n) - Count and compare approach. 2 strings will have the same set of alphabets
    repeated for the same number of times. Thus we we upgrade a list of 26
    counter each time a letter is found and in the end check if two list of
    counters are identical, then the strings must be anagram.
    """
    counter1 = [0]*26
    counter2 = [0]*26

    for i in range(len(s1)):
        pos = ord(s1[i]) - ord('a')
        counter1[pos] += 1

    for i in range(len(s2)):
        pos = ord(s1[i]) - ord('a')
        counter2[pos] += 1

    j = 0
    stillOk = True
    while j<26 and stillOk:
        if counter1[j] == counter2[j]:
            j += 1
        else:
            stillOk = False

    return stillOk


print anagram1('abcd', 'dcba')
print anagram2('abcd', 'dcba')
print anagram3('abcd', 'dcba')
